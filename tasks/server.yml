---
# server_side tasks
- name: openvpn | install packages
  ansible.builtin.apt:
    name:
      - openvpn
      - easy-rsa
      - iptables-persistent
    update_cache: yes
    state: present
  notify: 
    - enable openvpn

# set ip forwarding on in /proc and in the sysctl file and reload if necessary
- name: openvpn | enable ipv4 forwarding
  ansible.posix.sysctl:
    name: net.ipv4.ip_forward
    value: '1'
    sysctl_set: yes
    state: present
    reload: yes
  failed_when: false

- name: openvpn | create ipt ables nat chain
  ansible.builtin.iptables:
    table: nat
    chain: POSTROUTING
    jump: MASQUERADE
    protocol: all
    source: '0.0.0.0/0'
    destination: '0.0.0.0/0'
    comment: ansible nat masquerade
  become: true

- name: "openvpn | ensure {{ openvpn_dir }}/easyrsa exists"
  ansible.builtin.file:
    path: "{{ openvpn_dir }}/easy-rsa"
    state: directory
    mode: 0755
  become: true

- name: openvpn | create vars file
  ansible.builtin.copy:
    dest: "{{ openvpn_easyrsa_dir }}/vars"
    content: |
      set_var EASY_RSA_ALGO "ec"
      set_var EASY_RSA_DIGEST "sha512"

- name: openvpn | easy-rsa init-pki
  ansible.builtin.command:
    cmd: "{{ openvpn_easyrsa_dir }}/easyrsa init-pki"
    chdir: "{{ openvpn_dir }}/easy-rsa"
    creates: "{{ openvpn_dir }}/easy-rsa/pki"

- name: openvpn | easy-rsa build-ca
  ansible.builtin.command:
    cmd: "{{ openvpn_easyrsa_dir }}/easyrsa build-ca nopass"
    chdir: "{{ openvpn_dir }}/easy-rsa"
    creates: "{{ openvpn_dir }}/easy-rsa/pki/ca.crt"
  environment:
    EASYRSA_BATCH: "yes"

- name: openvpn | easy-rsa gen-dh
  ansible.builtin.command:
    cmd: "{{ openvpn_easyrsa_dir }}/easyrsa gen-dh"
    chdir: "{{ openvpn_dir }}/easy-rsa"
    creates: "{{ openvpn_dir }}/easy-rsa/pki/dh.pem"

- name: openvpn | easy-rsa server nopass
  ansible.builtin.command:
    cmd: "{{ openvpn_easyrsa_dir }}/easyrsa build-server-full server nopass"
    chdir: "{{ openvpn_dir }}/easy-rsa"
    creates: "{{ openvpn_dir }}/easy-rsa/pki/issued/server.crt"

- name: openvpn | easy-rsa client nopass
  ansible.builtin.command:
    cmd: "{{ openvpn_easyrsa_dir }}/easyrsa build-client-full {{ item }} nopass"
    chdir: "{{ openvpn_dir }}/easy-rsa"
    creates: "{{ openvpn_dir }}/easy-rsa/pki/issued/{{ item }}.crt"
  with_items: 
    - "{{ openvpn_clients }}"

- name: openvpn | easy-rsa gen-crl
  ansible.builtin.command:
    cmd: "{{ openvpn_easyrsa_dir }}/easyrsa gen-crl"
    chdir: "{{ openvpn_dir }}/easy-rsa"
    creates: "{{ openvpn_dir }}/easy-rsa/pki/issued/crl.pem"

- name: openvpn | openvpn genky 
  ansible.builtin.command:
    cmd: "openvpn --genkey --secret {{ openvpn_dir }}/easy-rsa/pki/ta.key"
    creates: "{{ openvpn_dir }}/easy-rsa/pki/ta.key"

- name: "openvpn | copy files to {{ openvpn_dir }}/server"
  ansible.builtin.copy:
    src: "{{ openvpn_dir }}/easy-rsa/pki/{{ item }}"
    dest: "{{ openvpn_dir }}/server/{{ item | basename }}"
    mode: "0644"
    remote_src: true
  loop:
    - ca.crt
    - dh.pem
    - ta.key
    - private/ca.key
    - private/server.key
    - issued/server.crt

- name: "openvpn | copy files to {{ openvpn_dir }}"
  ansible.builtin.copy:
    src: "{{ openvpn_dir }}/easy-rsa/pki/{{ item }}"
    dest: "{{ openvpn_dir }}/{{ item | basename }}"
    mode: "0644"
    remote_src: true
  loop:
    - ca.crt
    - ta.key

- name: "openvpn | place server configuration"
  ansible.builtin.template:
    src: "{{ role_path }}/templates/server.conf.j2" 
    dest: "{{ openvpn_dir }}/server.conf"
    owner: root
    group: root
    mode: "0644"
  notify:
    - restart openvpn
